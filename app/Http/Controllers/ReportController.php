<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function report(){
        $users = DB::table('user')
            ->count();
        $records = DB::table('record')
            ->select(DB::raw('sum(quantity) as total'))
            ->count();
        return view('report.report',[
            'users'=> $users,
            'records'=>$records
        ]);
    }


    public function table(){
        $usersQuery = DB::table('record');

        $isbn = (!empty($_GET["isbn"])) ? ($_GET["isbn"]) : ('');
        $title= (!empty($_GET["title"])) ? ($_GET["title"]) : ('');
        $book_class= (!empty($_GET["book_class"])) ? ($_GET["book_class"]) : ('');
        $date = (!empty($_GET["date"])) ? ($_GET["date"]) : ('');
        $from = (!empty($_GET["from"])) ? ($_GET["from"]) : ('');
        $to = (!empty($_GET["to"])) ? ($_GET["to"]) : ('');
        $print_status = (!empty($_GET["print_status"])) ? ($_GET["print_status"]) : ('');

        if($isbn){

            $iss = $isbn;
            $usersQuery->where("record_isbn",$iss);
        }
      if($title){
            $titles = $title;
            $usersQuery->where("record_name",$titles);
        }
      if($book_class){
            $classes = $book_class;
            $usersQuery->where("class_number",'=',$classes);
        }
      if($date){
            $dates = $date;
          $s = date('Y-m-d', strtotime($dates));
            $usersQuery->whereDate("timestamp",'=',$s);
        }
      if($from && $to){
          $first=$from;
          $second=$to;
          $ss = date('Y-m-d', strtotime($first));
          $sss = date('Y-m-d', strtotime($second));
            $usersQuery->whereBetween("timestamp",[$ss,$sss]);
        }
      if($print_status){
          if ($print_status==='green'){
              $print_green=$print_status;
              $usersQuery->where("print_status",$print_green);
          }
          else if($print_status==='black') {
              $print_black=$print_status;
              $usersQuery->where("print_status",$print_black);
          }

        }
        $users = $usersQuery->select('*');
        return datatables()->of($users)
            ->make(true);
    }


    public function userControl(){
        $usersQuery = DB::table('user');
        $users = $usersQuery->select('*');
        return datatables()->of($users)
            ->addColumn('action', function($row) {
                return '<a href="#'. $row->uid .'/edit" class="btn btn-primary"><i class="fas fa-pencil-alt" aria-hidden="true"></i> </a>'
                    /*'<a href="/delete/'. $row->uid .'" class="btn btn-danger"><i class="fas fa-times" aria-hidden="true"></i></a>'*/;
            })
            ->make(true);
    }
    public function delete($id) {
        $record = DB::table('user')->where('uid',$id);
        $record->delete();
        return redirect()->back();
    }

}
