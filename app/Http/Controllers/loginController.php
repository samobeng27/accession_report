<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;

class loginController extends Controller
{
    public function login(){
        return view('login');
    }

//    public function postLogin(Request $request){
//
//        //dd("sdsdsds");
//        $this->validate($request,[
//            'email'=>'required',
//            'password'=>'required',
//        ]);
//
//        if(!Auth::attempt(['email' =>$request->input('email'), 'password' =>$request->input('password')],$request->has('remember'))){
//
////            \Session::flash('flash_message','Your post has been saved !!!');
//            return redirect()->back();
//        }
//
//        return redirect('home');
//    }

//    public function postLogin(Request $request)
//    {
//        if (Auth::attempt(['uname' =>$request->input('uname'), 'pword' =>$request->input('pword')])) {
//            return redirect()->route('reports');
//
//        } else {
//            return redirect('login')->with('message', 'Login Failed');
//        }
//    }

//    public function postLogin(Request $request) {
//        //validate request
//        $this->validate($request, [
//            'login'    => 'required',
//            'pword' => 'required'
//        ]);
//
//        // get our login input
//        $login = $request->input('login');
//
//        // check login field
//        $login_type = filter_var( $login, FILTER_VALIDATE_EMAIL ) ? 'username' : 'uname';
//
//        // merge our login field into the request with either email or username as key
//        $request->merge([ $login_type => $login ]);
//
//        $credentials = $request->only($login_type, 'pword');
//        //check the user trying to login
//        if(isset($credentials['uname']) && !empty($credentials['uname']))
//            $user =DB::table('user')->where(['uname' => $credentials['uname']])->first();
//        else
//            $user = DB::table('user')->where(['username' => $credentials['username']])->first();
//
//
//        if (DB::table('user')->where($credentials, false))
//        {
//            $user = DB::table('user');
//            if (empty($user->wrong_password_attempt_count)) {
//                $user->wrong_password_attempt_count = 0;
//            }
//            if ($user->wrong_password_attempt_count > 0) {
//                $user->wrong_password_attempt_count = 0;
//                $user->save();
//            }
//
//            //hard coded to be removed later
////            if($user->uname == 'darthur') {
////                return redirect()->route('client.new_client');
////            }
//
//            if ($user === 0) {
//
//                //process change user password
////                dd('sam');
////                TODO: Change user password
////                abort(500, 'Your password has to be changed!');
//                return redirect()->route('app.password');
//            } else {
//                return redirect()->route('reports');
//            }
//        } else {
//            if (!empty($user)) {
//                if (empty($user->wrong_password_attempt_count)) {
//                    $user->wrong_password_attempt_count = 0;
//                }
//                $user->wrong_password_attempt_count++;
//                //lock user password after 21 trial
//                if ($user->wrong_password_attempt_count >= 30) {
//                    $user->is_locked = true;
//                }
//                $user->save();
//            }
//            return Redirect::back()
//                ->withInput($request->only('login', 'remember'))
//                ->withErrors('message','Incorrect Username or Password');
//        }
//    }


    public function postLogin(Request $request)
    {
        $users = DB::table('user')->where('uname', $request->uname)->first();

        if ( !$users ) { //If no employee was found
            return redirect()->back()->withErrors(['message' => 'No such Username ']);
        }
        else{
            return redirect()->route('reports');
        }

//        //Assuming you have the relationship setup between employee and user
//        //probably a one to one.
//        $user = DB::table('user')->select(DB::raw('pword as pwords'));
//
//        //manually check their password
//        //assuming you hash every password when saving the user
//        if (Hash::check($request->pword, $user->pword)) {
//            //password matched. Log in the user
//            Auth::login($user);
//            return redirect()->route('reports');
//        }

//        return redirect()->back()->withErrors(['credentials' => 'Check your credentials']);
    }
    public function logout() {
//        Auth::logout();
        return redirect('/');
    }
}
