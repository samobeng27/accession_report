@extends('layout.master-mini')
@section('content')

<div class="content-wrapper d-flex align-items-center justify-content-center auth theme-one" style="background-image: url({{ url('assets/images/auth/login_1.jpg') }}); background-size: cover;">
  <div class="row w-100">
    <div class="col-lg-4 mx-auto">
      <div class="auto-form-wrapper">

        <form method="post" action="{{route('post_login')}}">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{csrf_field()}}
          <div class="form-group">
            <label class="label">Username</label>
            <div class="input-group">
              <input type="text" class="form-control" name="uname" placeholder="Username" required>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="mdi mdi-check-circle-outline"></i>
                </span>
              </div>
            </div>
          </div>
{{--          <div class="form-group">--}}
{{--            <label class="label">Password</label>--}}
{{--            <div class="input-group">--}}
{{--              <input type="password" class="form-control" name="pword" placeholder="*********" required>--}}
{{--              <div class="input-group-append">--}}
{{--                <span class="input-group-text">--}}
{{--                  <i class="mdi mdi-check-circle-outline"></i>--}}
{{--                </span>--}}
{{--              </div>--}}
{{--            </div>--}}
{{--          </div>--}}
          <div class="form-group">
            <button type="submit" class="btn btn-primary submit-btn btn-block">Login</button>
          </div>

          <div class="text-block text-center my-3">
            <span class="text-small font-weight-semibold">copyright © 2020 <a href="https://www.library.gov.gh/" target="_blank">GhLA</a>. All rights reserved.</span>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
