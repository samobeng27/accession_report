@extends('layout.master')

@push('plugin-styles')
    <!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
{{--    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">--}}
@endpush

@section('content')
{{--    <div class="row">--}}
{{--        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">--}}
{{--            <div class="col-md-offset-1 card card-statistics">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">--}}
{{--                        <div class="float-left">--}}
{{--                            <i class="mdi mdi-cube text-danger icon-lg"></i>--}}
{{--                        </div>--}}
{{--                        <div class="float-right">--}}
{{--                            <p class="mb-0 text-right">Total Revenue</p>--}}
{{--                            <div class="fluid-container">--}}
{{--                                <h3 class="font-weight-medium text-right mb-0">$65,650</h3>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left">--}}
{{--                        <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> 65% lower growth </p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">--}}
{{--            <div class="card card-statistics">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">--}}
{{--                        <div class="float-left">--}}
{{--                            <i class="mdi mdi-receipt text-warning icon-lg"></i>--}}
{{--                        </div>--}}
{{--                        <div class="float-right">--}}
{{--                            <p class="mb-0 text-right">Orders</p>--}}
{{--                            <div class="fluid-container">--}}
{{--                                <h3 class="font-weight-medium text-right mb-0">3455</h3>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left">--}}
{{--                        <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> Product-wise sales </p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">--}}
{{--            <div class="card card-statistics">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">--}}
{{--                        <div class="float-left">--}}
{{--                            <i class="mdi mdi-poll-box text-success icon-lg"></i>--}}
{{--                        </div>--}}
{{--                        <div class="float-right">--}}
{{--                            <p class="mb-0 text-right">Sales</p>--}}
{{--                            <div class="fluid-container">--}}
{{--                                <h3 class="font-weight-medium text-right mb-0">5693</h3>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left">--}}
{{--                        <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> Weekly Sales </p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">--}}
{{--            <div class="card card-statistics">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">--}}
{{--                        <div class="float-left">--}}
{{--                            <i class="mdi mdi-account-box-multiple text-info icon-lg"></i>--}}
{{--                        </div>--}}
{{--                        <div class="float-right">--}}
{{--                            <p class="mb-0 text-right">Employees</p>--}}
{{--                            <div class="fluid-container">--}}
{{--                                <h3 class="font-weight-medium text-right mb-0">246</h3>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left">--}}
{{--                        <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Product-wise sales </p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-12 grid-margin">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="d-sm-flex justify-content-between align-items-center mb-4">--}}
{{--                        <h2 class="card-title mb-0">Product Analysis</h2>--}}
{{--                        <div class="wrapper d-flex">--}}
{{--                            <div class="d-flex align-items-center mr-3">--}}
{{--                                <span class="dot-indicator bg-success"></span>--}}
{{--                                <p class="mb-0 ml-2 text-muted">Product</p>--}}
{{--                            </div>--}}
{{--                            <div class="d-flex align-items-center">--}}
{{--                                <span class="dot-indicator bg-primary"></span>--}}
{{--                                <p class="mb-0 ml-2 text-muted">Resources</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="chart-container">--}}
{{--                        <canvas id="dashboard-area-chart" height="80"></canvas>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-6 col-xl-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <h4 class="card-title">Todo</h4>--}}
{{--                    <div class="add-items d-flex">--}}
{{--                        <input type="text" class="form-control todo-list-input" placeholder="What do you need to do today?">--}}
{{--                        <button class="add btn btn-primary font-weight-medium todo-list-add-btn">Add</button>--}}
{{--                    </div>--}}
{{--                    <div class="list-wrapper">--}}
{{--                        <ul class="d-flex flex-column-reverse todo-list todo-list-custom">--}}
{{--                            <li class="completed">--}}
{{--                                <div class="form-check form-check-flat">--}}
{{--                                    <label class="form-check-label">--}}
{{--                                        <input class="checkbox" type="checkbox" checked> Call John </label>--}}
{{--                                </div>--}}
{{--                                <i class="remove mdi mdi-close-circle-outline"></i>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <div class="form-check form-check-flat">--}}
{{--                                    <label class="form-check-label">--}}
{{--                                        <input class="checkbox" type="checkbox"> Create invoice </label>--}}
{{--                                </div>--}}
{{--                                <i class="remove mdi mdi-close-circle-outline"></i>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <div class="form-check form-check-flat">--}}
{{--                                    <label class="form-check-label">--}}
{{--                                        <input class="checkbox" type="checkbox"> Print Statements </label>--}}
{{--                                </div>--}}
{{--                                <i class="remove mdi mdi-close-circle-outline"></i>--}}
{{--                            </li>--}}
{{--                            <li class="completed">--}}
{{--                                <div class="form-check form-check-flat">--}}
{{--                                    <label class="form-check-label">--}}
{{--                                        <input class="checkbox" type="checkbox" checked> Prepare for presentation </label>--}}
{{--                                </div>--}}
{{--                                <i class="remove mdi mdi-close-circle-outline"></i>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-md-6 col-xl-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <h4 class="card-title">Schedules</h4>--}}
{{--                    <div class="shedule-list d-flex align-items-center justify-content-between mb-3">--}}
{{--                        <h3>27 Sep 2018</h3>--}}
{{--                        <small>21 Events</small>--}}
{{--                    </div>--}}
{{--                    <div class="event border-bottom py-3">--}}
{{--                        <p class="mb-2 font-weight-medium">Skype call with alex</p>--}}
{{--                        <div class="d-flex align-items-center">--}}
{{--                            <div class="badge badge-success">3:45 AM</div>--}}
{{--                            <small class="text-muted ml-2">London, UK</small>--}}
{{--                            <div class="image-grouped ml-auto">--}}
{{--                                <img src="{{ url('assets/images/faces/face10.jpg') }}" alt="profile image">--}}
{{--                                <img src="{{ url('assets/images/faces/face13.jpg') }}" alt="profile image"> </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="event py-3 border-bottom">--}}
{{--                        <p class="mb-2 font-weight-medium">Data Analysing with team</p>--}}
{{--                        <div class="d-flex align-items-center">--}}
{{--                            <div class="badge badge-warning">12.30 AM</div>--}}
{{--                            <small class="text-muted ml-2">San Francisco, CA</small>--}}
{{--                            <div class="image-grouped ml-auto">--}}
{{--                                <img src="{{ url('assets/images/faces/face20.jpg') }}" alt="profile image">--}}
{{--                                <img src="{{ url('assets/images/faces/face17.jpg') }}" alt="profile image">--}}
{{--                                <img src="{{ url('assets/images/faces/face14.jpg') }}" alt="profile image"> </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="event py-3">--}}
{{--                        <p class="mb-2 font-weight-medium">Meeting with client</p>--}}
{{--                        <div class="d-flex align-items-center">--}}
{{--                            <div class="badge badge-danger">4.15 AM</div>--}}
{{--                            <small class="text-muted ml-2">San Diego, CA</small>--}}
{{--                            <div class="image-grouped ml-auto">--}}
{{--                                <img src="{{ url('assets/images/faces/face21.jpg') }}" alt="profile image">--}}
{{--                                <img src="{{ url('assets/images/faces/face16.jpg') }}" alt="profile image"> </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="col-md-6 col-xl-6 grid-margin stretch-card">
            <div class="row flex-grow">
                <div class="col-md-6 col-xl-8 grid-margin grid-margin-md-0 grid-margin-xl stretch-card">
                    <div class="card card-revenue">
                        <div class="card-body d-flex align-items-center">
                            <div class="d-flex flex-grow">
                                <div class="mr-auto">
                                    <p class="highlight-text mb-0 text-white"> {{ number_format ($records)}}</p>
                                    <p class="text-white"> Books </p>
{{--                                    <div class="badge badge-pill"> 18% </div>--}}
                                </div>
                                <div class="ml-auto align-self-end">
{{--                                    <div id="revenue-chart" sparkType="bar" sparkBarColor="#e6ecf5" barWidth="2"> <i class="mdi mdi-account-outline mr-0 text-gray"></i> </div>--}}
                                    <i class="fas fa-book fa-3x" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-revenue" style="margin-left: 10px !important;">
                        <div class="card-body d-flex align-items-center">
                            <div class="d-flex flex-grow">
                                <div class="mr-auto">
                                    <p class="highlight-text mb-0 text-white"> {{$users}}</p>
                                    <p class="text-white"> Users </p>
{{--                                    <div class="badge badge-pill"> 18% </div>--}}
                                </div>
                                <div class="ml-auto align-self-end">
{{--                                    <div id="revenue-chart" sparkType="bar" sparkBarColor="#e6ecf5" barWidth="2"> 4,3,10,9,4,3,8,6,7,8 </div>--}}
                                    <i class="fas fa-users fa-3x" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

{{--                <button type="button" class="form-control btn btn-primary" data-toggle="modal" data-target="#myModal">Accession Register Users</button>--}}


{{--                <div class="col-md-6 col-xl-12 stretch-card">--}}
{{--                    <div class="card card-revenue-table">--}}
{{--                        <div class="card-body">--}}
{{--                            <div class="revenue-item d-flex">--}}
{{--                                <div class="revenue-desc">--}}
{{--                                    <h6>Member Profit</h6>--}}
{{--                                    <p class="font-weight-light"> Average Weekly Profit </p>--}}
{{--                                </div>--}}
{{--                                <div class="revenue-amount">--}}
{{--                                    <p class="text-primary"> +168.900 </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="revenue-item d-flex">--}}
{{--                                <div class="revenue-desc">--}}
{{--                                    <h6>Total Profit</h6>--}}
{{--                                    <p class="font-weight-light"> Weekly Customer Orders </p>--}}
{{--                                </div>--}}
{{--                                <div class="revenue-amount">--}}
{{--                                    <p class="text-primary"> +6890.00 </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>


</div>
{{--    </div>--}}
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <label for=""style="color: black">Report Type</label>
                <select name="report_type" id="report_type" class="selected col-md-6">
                    <option value="">-select report type-</option>
                    <option value="date">Date</option>
                    <option value="between_date">Between Date</option>
                    <option value="book_class">Book Class</option>
                    <option value="title">Title</option>
                    <option value="isbn">ISBN</option>
                    <option value="status">Print Status</option>

                </select>

        </div>
        </div>

    </div>
</div>


<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
{{--                <form method="POST" id="search-form" action="" >--}}

                <div class="div_date" id="date">
                    <label for="date">Date</label>
                    <input type="date" class="date form-control col-md-6" id="date" name="date">

                </div>
                <div class="row div_b_date" id="b_date">
                    <div class="col-md-6">
                        <label for="From">From</label>
                        <input type="date" class="from form-control" id="from" name="from">
                    </div>
                    <div class="col-md-6">
                        <label for="From">To</label>
                        <input type="date" class="to form-control" id="to" name="to">
                    </div>


                </div>
                <div class="div_book_class" id="book_class">
                    <label for="From">Book Class</label>
                    <input type="text" class="book_class form-control col-md-6" name="book_class">
                </div>
                <div class="div_title" id="title">
                    <label for="From">Title</label>
                    <input type="text" class="title form-control col-md-6" id="title" name="title">
                </div>
                <div class="div_isbn" id="isbn">
                    <label for="From">ISBN</label>
                    <input type="text" class="isbn form-control" id="isbn" name="isbn">
{{--                    <input type="text" id="test" name="test" class="test">--}}

                </div>
                <div class="div_print" id="print">
                    <label for="status">Books with Printed Barcodes</label>
                    <select name="print_status" id="print_status" class="print_status selected col-md-6">
                        <option value="">-Select Status-</option>
                        <option value="green">Generated</option>
                        <option value="black">Not Generated</option>
                    </select>
                </div>
                    <br>
                    <div class="gen row" id="gen">
                        <div class="col-md-6">

                        </div>
                        <div class="col-md-6">
                            <button style="margin-top: 25px" id="searchBt" name="searchBt" value="1" class="form-control btn btn-primary"><i class="fa fa-search">  Search</i> </button>

                        </div>
                </div>
                <br>
                <div class="row">

                    <div class="col-md-12">
                        <table id="datatable" class="table table-hover table-bordered table-striped datatable" width="100%">
                            <thead>
                            <tr>
                                {{--                            <th>Id</th>--}}
                                <th>ISBN</th>
                                <th >Title</th>
                                <th >Class Number</th>
                                <th >Quantity</th>
                                <th >Print Status</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                        </table>
                            {{csrf_field()}}

{{--                        </form>--}}
                    </div>

                </div>

        </div>
        </div>

    </div>
{{--    <input type="text" id="test" name="test">--}}
</div>
{{--    <div class="row">--}}
{{--        <div class="col-sm-6 col-md-6 col-lg-6 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-5 d-flex align-items-center">--}}
{{--                            <canvas id="UsersDoughnutChart" class="400x160 mb-4 mb-md-0" height="200"></canvas>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-7">--}}
{{--                            <h4 class="card-title font-weight-medium mb-0 d-none d-md-block">Active Users</h4>--}}
{{--                            <div class="wrapper mt-4">--}}
{{--                                <div class="d-flex justify-content-between mb-2">--}}
{{--                                    <div class="d-flex align-items-center">--}}
{{--                                        <p class="mb-0 font-weight-medium">67,550</p>--}}
{{--                                        <small class="text-muted ml-2">Email account</small>--}}
{{--                                    </div>--}}
{{--                                    <p class="mb-0 font-weight-medium">80%</p>--}}
{{--                                </div>--}}
{{--                                <div class="progress">--}}
{{--                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 88%" aria-valuenow="88" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="wrapper mt-4">--}}
{{--                                <div class="d-flex justify-content-between mb-2">--}}
{{--                                    <div class="d-flex align-items-center">--}}
{{--                                        <p class="mb-0 font-weight-medium">21,435</p>--}}
{{--                                        <small class="text-muted ml-2">Requests</small>--}}
{{--                                    </div>--}}
{{--                                    <p class="mb-0 font-weight-medium">34%</p>--}}
{{--                                </div>--}}
{{--                                <div class="progress">--}}
{{--                                    <div class="progress-bar bg-success" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-sm-6 col-md-6 col-lg-6 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-7">--}}
{{--                            <h4 class="card-title font-weight-medium mb-3">Amount Due</h4>--}}
{{--                            <h1 class="font-weight-medium mb-0">$5998</h1>--}}
{{--                            <p class="text-muted">Milestone Completed</p>--}}
{{--                            <p class="mb-0">Payment for next week</p>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-5 d-flex align-items-end mt-4 mt-md-0">--}}
{{--                            <canvas id="conversionBarChart" height="150"></canvas>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-md-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body py-5">--}}
{{--                    <div class="d-flex flex-row justify-content-center align-items">--}}
{{--                        <i class="mdi mdi-facebook text-facebook icon-lg"></i>--}}
{{--                        <div class="ml-3">--}}
{{--                            <h6 class="text-facebook font-weight-semibold mb-0">2.62 Subscribers</h6>--}}
{{--                            <p class="text-muted card-text">You main list growing</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-md-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body py-5">--}}
{{--                    <div class="d-flex flex-row justify-content-center align-items">--}}
{{--                        <i class="mdi mdi-google-plus text-google icon-lg"></i>--}}
{{--                        <div class="ml-3">--}}
{{--                            <h6 class="text-google font-weight-semibold mb-0">3.4k Followers</h6>--}}
{{--                            <p class="text-muted card-text">You main list growing</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-md-4 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body py-5">--}}
{{--                    <div class="d-flex flex-row justify-content-center align-items">--}}
{{--                        <i class="mdi mdi-twitter text-twitter icon-lg"></i>--}}
{{--                        <div class="ml-3">--}}
{{--                            <h6 class="text-twitter font-weight-semibold mb-0">3k followers</h6>--}}
{{--                            <p class="text-muted card-text">You main list growing</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row">--}}
{{--        <div class="col-lg-12 grid-margin">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <h4 class="card-title">Orders</h4>--}}
{{--                    <div class="table-responsive">--}}
{{--                        <table class="table table-striped">--}}
{{--                            <thead>--}}
{{--                            <tr>--}}
{{--                                <th> # </th>--}}
{{--                                <th> First name </th>--}}
{{--                                <th> Progress </th>--}}
{{--                                <th> Amount </th>--}}
{{--                                <th> Sales </th>--}}
{{--                                <th> Deadline </th>--}}
{{--                            </tr>--}}
{{--                            </thead>--}}
{{--                            <tbody>--}}
{{--                            <tr>--}}
{{--                                <td class="font-weight-medium"> 1 </td>--}}
{{--                                <td> Herman Beck </td>--}}
{{--                                <td>--}}
{{--                                    <div class="progress">--}}
{{--                                        <div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                                <td> $ 77.99 </td>--}}
{{--                                <td class="text-danger"> 53.64% <i class="mdi mdi-arrow-down"></i>--}}
{{--                                </td>--}}
{{--                                <td> May 15, 2015 </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td class="font-weight-medium"> 2 </td>--}}
{{--                                <td> Messsy Adam </td>--}}
{{--                                <td>--}}
{{--                                    <div class="progress">--}}
{{--                                        <div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                                <td> $245.30 </td>--}}
{{--                                <td class="text-success"> 24.56% <i class="mdi mdi-arrow-up"></i>--}}
{{--                                </td>--}}
{{--                                <td> July 1, 2015 </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td class="font-weight-medium"> 3 </td>--}}
{{--                                <td> John Richards </td>--}}
{{--                                <td>--}}
{{--                                    <div class="progress">--}}
{{--                                        <div class="progress-bar bg-warning progress-bar-striped" role="progressbar" style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                                <td> $138.00 </td>--}}
{{--                                <td class="text-danger"> 28.76% <i class="mdi mdi-arrow-down"></i>--}}
{{--                                </td>--}}
{{--                                <td> Apr 12, 2015 </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td class="font-weight-medium"> 4 </td>--}}
{{--                                <td> Peter Meggik </td>--}}
{{--                                <td>--}}
{{--                                    <div class="progress">--}}
{{--                                        <div class="progress-bar bg-primary progress-bar-striped" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                                <td> $ 77.99 </td>--}}
{{--                                <td class="text-danger"> 53.45% <i class="mdi mdi-arrow-down"></i>--}}
{{--                                </td>--}}
{{--                                <td> May 15, 2015 </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td class="font-weight-medium"> 5 </td>--}}
{{--                                <td> Edward </td>--}}
{{--                                <td>--}}
{{--                                    <div class="progress">--}}
{{--                                        <div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: 35%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                                <td> $ 160.25 </td>--}}
{{--                                <td class="text-success"> 18.32% <i class="mdi mdi-arrow-up"></i>--}}
{{--                                </td>--}}
{{--                                <td> May 03, 2015 </td>--}}
{{--                            </tr>--}}
{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row">--}}
{{--        <div class="col-12">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    <h5 class="card-title mb-4">Manage Tickets</h5>--}}
{{--                    <div class="fluid-container">--}}
{{--                        <div class="row ticket-card mt-3 pb-2 border-bottom pb-3 mb-3">--}}
{{--                            <div class="col-md-1">--}}
{{--                                <img class="img-sm rounded-circle mb-4 mb-md-0 d-block mx-md-auto" src="{{ url('assets/images/faces/face1.jpg') }}" alt="profile image"> </div>--}}
{{--                            <div class="ticket-details col-md-9">--}}
{{--                                <div class="d-flex">--}}
{{--                                    <p class="text-dark font-weight-semibold mr-2 mb-0 no-wrap">James :</p>--}}
{{--                                    <p class="text-primary mr-1 mb-0">[#23047]</p>--}}
{{--                                    <p class="mb-0 ellipsis">Donec rutrum congue leo eget malesuada.</p>--}}
{{--                                </div>--}}
{{--                                <p class="text-gray ellipsis mb-2">Donec rutrum congue leo eget malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim vivamus. </p>--}}
{{--                                <div class="row text-gray d-md-flex d-none">--}}
{{--                                    <div class="col-4 d-flex">--}}
{{--                                        <small class="mb-0 mr-2 text-muted text-muted">Last responded :</small>--}}
{{--                                        <small class="Last-responded mr-2 mb-0 text-muted text-muted">3 hours ago</small>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-4 d-flex">--}}
{{--                                        <small class="mb-0 mr-2 text-muted text-muted">Due in :</small>--}}
{{--                                        <small class="Last-responded mr-2 mb-0 text-muted text-muted">2 Days</small>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="ticket-actions col-md-2">--}}
{{--                                <div class="btn-group dropdown">--}}
{{--                                    <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Manage </button>--}}
{{--                                    <div class="dropdown-menu">--}}
{{--                                        <a class="dropdown-item" href="#">--}}
{{--                                            <i class="fa fa-reply fa-fw"></i>Quick reply</a>--}}
{{--                                        <a class="dropdown-item" href="#">--}}
{{--                                            <i class="fa fa-history fa-fw"></i>Another action</a>--}}
{{--                                        <div class="dropdown-divider"></div>--}}
{{--                                        <a class="dropdown-item" href="#">--}}
{{--                                            <i class="fa fa-check text-success fa-fw"></i>Resolve Issue</a>--}}
{{--                                        <a class="dropdown-item" href="#">--}}
{{--                                            <i class="fa fa-times text-danger fa-fw"></i>Close Issue</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row ticket-card mt-3 pb-2 border-bottom pb-3 mb-3">--}}
{{--                            <div class="col-md-1">--}}
{{--                                <img class="img-sm rounded-circle mb-4 mb-md-0 d-block mx-md-auto" src="{{ url('assets/images/faces/face2.jpg') }}" alt="profile image"> </div>--}}
{{--                            <div class="ticket-details col-md-9">--}}
{{--                                <div class="d-flex">--}}
{{--                                    <p class="text-dark font-weight-semibold mr-2 mb-0 no-wrap">Stella :</p>--}}
{{--                                    <p class="text-primary mr-1 mb-0">[#23135]</p>--}}
{{--                                    <p class="mb-0 ellipsis">Curabitur aliquet quam id dui posuere blandit.</p>--}}
{{--                                </div>--}}
{{--                                <p class="text-gray ellipsis mb-2">Pellentesque in ipsum id orci porta dapibus. Sed porttitor lectus nibh. Curabitur non nulla sit amet nisl. </p>--}}
{{--                                <div class="row text-gray d-md-flex d-none">--}}
{{--                                    <div class="col-4 d-flex">--}}
{{--                                        <small class="mb-0 mr-2 text-muted">Last responded :</small>--}}
{{--                                        <small class="Last-responded mr-2 mb-0 text-muted">3 hours ago</small>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-4 d-flex">--}}
{{--                                        <small class="mb-0 mr-2 text-muted">Due in :</small>--}}
{{--                                        <small class="Last-responded mr-2 mb-0 text-muted">2 Days</small>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="ticket-actions col-md-2">--}}
{{--                                <div class="btn-group dropdown">--}}
{{--                                    <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Manage </button>--}}
{{--                                    <div class="dropdown-menu">--}}
{{--                                        <a class="dropdown-item" href="#">--}}
{{--                                            <i class="fa fa-reply fa-fw"></i>Quick reply</a>--}}
{{--                                        <a class="dropdown-item" href="#">--}}
{{--                                            <i class="fa fa-history fa-fw"></i>Another action</a>--}}
{{--                                        <div class="dropdown-divider"></div>--}}
{{--                                        <a class="dropdown-item" href="#">--}}
{{--                                            <i class="fa fa-check text-success fa-fw"></i>Resolve Issue</a>--}}
{{--                                        <a class="dropdown-item" href="#">--}}
{{--                                            <i class="fa fa-times text-danger fa-fw"></i>Close Issue</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row ticket-card mt-3">--}}
{{--                            <div class="col-md-1">--}}
{{--                                <img class="img-sm rounded-circle mb-4 mb-md-0 d-block mx-md-auto" src="{{ url('assets/images/faces/face3.jpg') }}" alt="profile image"> </div>--}}
{{--                            <div class="ticket-details col-md-9">--}}
{{--                                <div class="d-flex">--}}
{{--                                    <p class="text-dark font-weight-semibold mr-2 mb-0 no-wrap">John Doe :</p>--}}
{{--                                    <p class="text-primary mr-1 mb-0">[#23246]</p>--}}
{{--                                    <p class="mb-0 ellipsis">Mauris blandit aliquet elit, eget tincidunt nibh pulvinar.</p>--}}
{{--                                </div>--}}
{{--                                <p class="text-gray ellipsis mb-2">Nulla quis lorem ut libero malesuada feugiat. Proin eget tortor risus. Lorem ipsum dolor sit amet.</p>--}}
{{--                                <div class="row text-gray d-md-flex d-none">--}}
{{--                                    <div class="col-4 d-flex">--}}
{{--                                        <small class="mb-0 mr-2 text-muted">Last responded :</small>--}}
{{--                                        <small class="Last-responded mr-2 mb-0 text-muted">3 hours ago</small>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-4 d-flex">--}}
{{--                                        <small class="mb-0 mr-2 text-muted">Due in :</small>--}}
{{--                                        <small class="Last-responded mr-2 mb-0 text-muted">2 Days</small>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="ticket-actions col-md-2">--}}
{{--                                <div class="btn-group dropdown">--}}
{{--                                    <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Manage </button>--}}
{{--                                    <div class="dropdown-menu">--}}
{{--                                        <a class="dropdown-item" href="#">--}}
{{--                                            <i class="fa fa-reply fa-fw"></i>Quick reply</a>--}}
{{--                                        <a class="dropdown-item" href="#">--}}
{{--                                            <i class="fa fa-history fa-fw"></i>Another action</a>--}}
{{--                                        <div class="dropdown-divider"></div>--}}
{{--                                        <a class="dropdown-item" href="#">--}}
{{--                                            <i class="fa fa-check text-success fa-fw"></i>Resolve Issue</a>--}}
{{--                                        <a class="dropdown-item" href="#">--}}
{{--                                            <i class="fa fa-times text-danger fa-fw"></i>Close Issue</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
<div class="modal fade" id="myModal">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="">Accession Register Users</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <table id="users_data" class="table table-hover table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th >Username</th>
                        <th >Action</th>

                    </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@endsection

@push('plugin-scripts')
    {!! Html::script('/assets/plugins/chartjs/chart.min.js') !!}
    {!! Html::script('/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
{{--    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/responsive/1.0.2/js/dataTables.responsive.js"></script>--}}
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
{{--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>--}}
    <script>

        {{--$('#searchBt').on('click',function () {--}}
        {{--    // alert('sam');--}}
        {{--    // var test=$('#test').val().trim();--}}
        {{--    var date=$('.date').val().trim();--}}
        {{--    var from=$('.from').val().trim();--}}
        {{--    var to=$('.to').val().trim();--}}
        {{--    var title=$('.title').val().trim();--}}
        {{--    var isbn=$('.isbn').val().trim();--}}
        {{--    console.log(isbn);--}}
        {{--    var book_class=$('.book_class').val().trim();--}}
        {{--    var print_status=$('.print_status').val().trim();--}}

        {{--    if(date==""||date==" "){--}}

        {{--    }--}}
        {{--    else if(from==""||from==" ") {--}}

        {{--    }--}}
        {{--    else if(to==""||to==" ") {--}}

        {{--    }--}}
        {{--    else if(title==""||title==" ") {--}}

        {{--    }--}}
        {{--    else if(isbn==""||isbn==" ") {--}}

        {{--    }--}}
        {{--    else if(book_class==""||book_class==" ") {--}}

        {{--    }--}}
        {{--    else if(print_status==""||print_status==" ") {--}}

        {{--    }--}}
        {{--    else {--}}
        {{--        var   url=  '{!! route('data') !!}';--}}
        {{--        var  data= {--}}
        {{--            'date' : date,--}}
        {{--            'from' : from,--}}
        {{--            'to' : to,--}}
        {{--            'title' : title,--}}
        {{--            'isbn' : isbn,--}}
        {{--            'book_class' : book_class,--}}
        {{--            'print_status' : print_status,--}}
        {{--        }--}}
        {{--        tab(url,data);--}}
        {{--        // alert(datepicker)--}}
        {{--        //submit--}}
        {{--    }--}}
        {{--});--}}

        {{--var   url=  '{!! route('data') !!}';--}}
        {{--function tab(url,data) {--}}
        {{--    $('#datatable').DataTable({--}}
        {{--        processing: true,--}}
        {{--        serverSide: true,--}}
        {{--        responsive: true,--}}
        {{--        dom: 'Blfrtip',--}}
        {{--        destroy: true,--}}
        {{--        ajax: {url:url,type:'POST','headers': {'X-CSRF-TOKEN': '{{ csrf_token() }}'--}}
        {{--            }, 'data':data},--}}
        {{--        //--}}
        {{--        columns: [--}}
        {{--            {data: 'record_isbn', name: 'record_isbn',orderable: true, searchable: true },--}}
        {{--            {data: 'record_name', name: 'record_name',orderable: true, searchable: true},--}}
        {{--            {data: 'class_number', name: 'class_number',orderable: true, searchable: true},--}}
        {{--            {data: 'quantity', name: 'quantity',orderable: true, searchable: true},--}}
        {{--            // {data: 'pid', name: 'pid',orderable: true, searchable: true},--}}
        {{--            {data: 'print_status', name: 'print_status',orderable: true, searchable: true},--}}
        {{--            {data: 'timestamp', name: 'timestamp',orderable: true, searchable: true},--}}
        {{--        ],--}}
        {{--    });--}}
        {{--}--}}
        {{--$(function() {--}}
        {{--    $('#datatable').DataTable({--}}
        {{--        processing: true,--}}
        {{--        serverSide: true,--}}
        {{--        dom: 'Bfrtip',--}}
        {{--        ajax: {url:'{!! route('data') !!}',type:'POST','headers': {'X-CSRF-TOKEN': '{{ csrf_token() }}'--}}
        {{--            }},--}}
        {{--        columns: [--}}
        {{--            {data: 'record_isbn', name: 'record_isbn' },--}}
        {{--            {data: 'record_name', name: 'record_name'},--}}
        {{--            {data: 'class_number', name: 'class_number'},--}}
        {{--            {data: 'quantity', name: 'quantity'},--}}
        {{--            // {data: 'pid', name: 'pid',orderable: true, searchable: true},--}}
        {{--            {data: 'print_status', name: 'print_status'},--}}
        {{--            {data: 'timestamp', name: 'timestamp'},--}}
        {{--        ],--}}
        {{--        // buttons: [--}}
        {{--        //     {--}}
        {{--        //         //--}}
        {{--        //         extend: 'copyHtml5',--}}
        {{--        //         title:'Auction Category',--}}
        {{--        //         exportOptions: {--}}
        {{--        //             columns: [ 0,1 ]--}}
        {{--        //         }--}}
        {{--        //     },--}}
        {{--        //     {--}}
        {{--        //         //--}}
        {{--        //         extend: 'csvHtml5',--}}
        {{--        //         title:'Auction Category',--}}
        {{--        //         exportOptions: {--}}
        {{--        //             columns: [ 0,1 ]--}}
        {{--        //         }--}}
        {{--        //     },--}}
        {{--        //     {--}}
        {{--        //         //--}}
        {{--        //         extend: 'excelHtml5',--}}
        {{--        //         title:'Auction Category',--}}
        {{--        //         exportOptions: {--}}
        {{--        //             columns: [ 0,1]--}}
        {{--        //         }--}}
        {{--        //     },--}}
        {{--        //     {--}}
        {{--        //         //--}}
        {{--        //         extend: 'pdfHtml5',--}}
        {{--        //         title:'Auction Category',--}}
        {{--        //         exportOptions: {--}}
        {{--        //             columns: [ 0,1 ]--}}
        {{--        //         }--}}
        {{--        //     },--}}
        {{--        //     {--}}
        {{--        //         //--}}
        {{--        //         extend: 'print',--}}
        {{--        //         title:'Auction Category',--}}
        {{--        //         exportOptions: {--}}
        {{--        //             columns: [ 0,1 ]--}}
        {{--        //         }--}}
        {{--        //     },--}}
        {{--        // ]--}}
        {{--    });--}}
        {{--    $('.input-sm').attr('placeholder','Search...');--}}
        {{--    $('.input-sm').addClass('form-control');--}}
        {{--    $('.dt-buttons').addClass('btn-group');--}}
        {{--    $('.dt-buttons a').addClass('btn btn-info');--}}
        {{--});--}}
    </script>
    <script>
        $(document).ready(function (){
            $("#date").hide();
            $("#b_date").hide();
            $("#book_class").hide();
            $("#title").hide();
            $("#isbn").hide();
            $("#print").hide();
            $("#gen").hide();
        })

        $(document).ready(function(){
            $('#report_type').on('change', function() {
                if ( this.value == 'date')
                {
                    $("#date").show();
                    $("#b_date").hide();
                    $("#book_class").hide();
                    $("#title").hide();
                    $("#isbn").hide();
                    $("#print").hide();
                    $("#gen").show();

                    $(".b_date").val('');
                    $(".book_class").val('');
                    $(".title").val('');
                    $(".isbn").val('');
                    $(".print_status").val('');
                    $(".to").val('');
                    $(".from").val('');
                }
                else if(this.value == 'between_date'){
                    $("#b_date").show();
                    $("#date").hide();
                    $("#book_class").hide();
                    $("#title").hide();
                    $("#isbn").hide();
                    $("#print").hide();
                    $("#gen").show();

                    $(".date").val('');
                    $(".book_class").val('');
                    $(".title").val('');
                    $(".isbn").val('');
                    $(".print_status").val('');
                    $(".to").val('');
                    $(".from").val('');
                }
                else if(this.value == 'book_class'){
                    $("#b_date").hide();
                    $("#date").hide();
                    $("#book_class").show();
                    $("#title").hide();
                    $("#isbn").hide();
                    $("#print").hide();
                    $("#gen").show();

                    $(".b_date").val('');
                    $(".date").val('');
                    $(".title").val('');
                    $(".isbn").val('');
                    $(".print_status").val('');
                    $(".to").val('');
                    $(".from").val('');
                }
                else if(this.value == 'title'){
                    $("#b_date").hide();
                    $("#date").hide();
                    $("#book_class").hide();
                    $("#title").show();
                    $("#isbn").hide();
                    $("#print").hide();
                    $("#gen").show();

                    $(".b_date").val('');
                    $(".date").val('');
                    $(".isbn").val('');
                    $(".print_status").val('');
                    $(".to").val('');
                    $(".from").val('');
                    $(".book_class").val('');
                }
                else if(this.value == 'isbn'){
                    $("#date").hide();
                    $("#book_class").hide();
                    $("#title").hide();
                    $("#isbn").show();
                    $("#print").hide();
                    $("#gen").show();
                    $("#b_date").hide();

                    $(".b_date").val('');
                    $(".date").val('');
                    $(".print_status").val('');
                    $(".title").val('');
                    $(".to").val('');
                    $(".from").val('');
                    $(".book_class").val('');
                }
                else if(this.value == 'status'){
                    $("#date").hide();
                    $("#book_class").hide();
                    $("#title").hide();
                    $("#isbn").hide();
                    $("#print").show();
                    $("#gen").show();
                    $("#b_date").hide();

                    $(".b_date").val('');
                    $(".date").val('');
                    $(".title").val('');
                    $(".isbn").val('');
                    $(".to").val('');
                    $(".from").val('');
                    $(".book_class").val('');
                }
                else
                {
                    $("#date").hide();
                    $("#b_date").hide();
                    $("#book_class").hide();
                    $("#title").hide();
                    $("#isbn").hide();
                    $("#print").hide();
                    $("#gen").hide();
                }
            });
        });

        $(document).ready( function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#datatable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                // dom: 'Bfrtip',
                // "scrollY": 250,
                scrollX: true,
                dom: 'Bfltip',

                ajax: {
                    url: "{{ url('tabless') }}",
                    type: 'GET',
                    data: function (d) {
                        d.isbn = $('.isbn').val();
                        d.title = $('.title').val();
                        d.book_class = $('.book_class').val();
                        d.date = $('.date').val();
                        d.from = $('.from').val();
                        d.to = $('.to').val();
                       d.print_status= $(".print_status").val();

                        console.log(d.print_status)
                    }
                },
                buttons: [
                    'copy','excel','csv','pdf'
                ],
                columns: [
                    {data: 'record_isbn', name: 'record_isbn' },
                    {data: 'record_name', name: 'record_name'},
                    {data: 'class_number', name: 'class_number'},
                    {data: 'quantity', name: 'quantity'},
                    // {data: 'pid', name: 'pid',orderable: true, searchable: true},
                    {data: 'print_status', name: 'print_status'},
                    {data: 'timestamp', name: 'timestamp'},
                ],
            });
        });

        $('#searchBt').click(function(){
          var dd=  $('#datatable').DataTable().draw(true);
        });

        //users
        $(document).ready( function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#users_data').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                // dom: 'Bfrtip',
                // "scrollY": 250,
                // scrollX: true,
                dom: 'Bfltip',

                ajax: {
                    url: "{{ url('users') }}",
                    type: 'GET',
                },
                buttons: [
                    'copy','excel','csv','pdf'
                ],
                columns: [
                    {data: 'name', name: 'name' },
                    {data: 'uname', name: 'uname'},
                    {data: 'action', name: 'action'},

                ],
            });
        });
    </script>
@endpush

@push('custom-scripts')
    {!! Html::script('/assets/js/dashboard.js') !!}
@endpush
